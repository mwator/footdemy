package pl.mwator.footdemy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mwator.footdemy.model.Post;
import pl.mwator.footdemy.service.PostService;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/post")
public class PostController {
    private final PostService postService;

    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }

    @RequestMapping(value = "/get/all", method = RequestMethod.GET)
    public ResponseEntity<?> getPosts(){
        return new ResponseEntity<Object>(postService.getPosts(), HttpStatus.OK);
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getPost(@PathVariable int id){
        return new ResponseEntity<Object>(postService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deletePost(@PathVariable int id){
        postService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<?> createPost(@RequestBody Post post){
        postService.create(post);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> editPost(@PathVariable int id, @RequestBody Post post){
        System.out.println("Updating Post " + id);

        Post currentPost = postService.get(id);

        if (currentPost == null) {
            System.out.println("Post with id " + id + " not found");
            return new ResponseEntity<Post>(HttpStatus.NOT_FOUND);
        }

        currentPost.setTitle(post.getTitle());
        currentPost.setMessage(post.getMessage());


        postService.update(currentPost);

        return new ResponseEntity<>(postService.update(currentPost), HttpStatus.OK);
    }
}

