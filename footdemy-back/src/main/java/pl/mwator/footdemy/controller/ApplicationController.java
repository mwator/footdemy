package pl.mwator.footdemy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mwator.footdemy.model.Application;
import pl.mwator.footdemy.service.ApplicationService;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/application")
public class ApplicationController {
    private final ApplicationService applicationService;

    @Autowired
    public ApplicationController(ApplicationService applicationService) {
        this.applicationService = applicationService;
    }

    @RequestMapping(value = "/get/all", method = RequestMethod.GET)
    public ResponseEntity<?> getApplications(){
        return new ResponseEntity<Object>(applicationService.getApplications(), HttpStatus.OK);
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getApplication(@PathVariable int id){
        return new ResponseEntity<Object>(applicationService.get(id), HttpStatus.OK);
    }
    @RequestMapping(value = "/get/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteApplication(@PathVariable int id){
        applicationService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<?> createApplication(@RequestBody Application application){
        applicationService.create(application);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

}
