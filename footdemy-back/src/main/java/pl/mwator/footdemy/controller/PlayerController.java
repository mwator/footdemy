package pl.mwator.footdemy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mwator.footdemy.model.Player;
import pl.mwator.footdemy.service.PlayerService;
import pl.mwator.footdemy.service.TeamService;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/player")
public class PlayerController {
    private final PlayerService playerService;
    private final TeamService teamService;

    @Autowired
    public PlayerController(PlayerService playerService, TeamService teamService) {
        this.playerService = playerService;
        this.teamService = teamService;
    }

    @RequestMapping(value = "/get/all", method = RequestMethod.GET)
    public ResponseEntity<?> getPlayers(){
        return new ResponseEntity<Object>(playerService.getPlayers(), HttpStatus.OK);
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getPlayer(@PathVariable int id){
        return new ResponseEntity<Object>(playerService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deletePlayer(@PathVariable int id){
        playerService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @RequestMapping(value = "/create", method = RequestMethod.POST )
    public ResponseEntity<?> createPlayer(@RequestBody Player player){
        /*System.out.println(player.getFirstName());
        System.out.println(player.getTeam().getId());*/
        playerService.create(player);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/getplayersbyteam/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getPlayersByTeam(@PathVariable int id){
        return new ResponseEntity<Object>(playerService.findPlayersByTeam_Id(id), HttpStatus.OK);
    }
    @RequestMapping(value = "/getplayersbyteam2/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getPlayersByTeam2(@PathVariable int id){
        return new ResponseEntity<Object>(playerService.findPlayersByTeamIdIs(id), HttpStatus.OK);
    }
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.PUT )
    public ResponseEntity<?> editPlayer(@PathVariable int id, @RequestBody Player player){
        System.out.println("Updating User " + id);

        Player currentPlayer = playerService.get(id);

        if (currentPlayer==null) {
            System.out.println("User with id " + id + " not found");
            return new ResponseEntity<Player>(HttpStatus.NOT_FOUND);
        }

        currentPlayer.setFirstName(player.getFirstName());
        currentPlayer.setLastName(player.getLastName());


        playerService.update(currentPlayer);

        return new ResponseEntity<>(playerService.update(currentPlayer), HttpStatus.OK);
    }

}
