package pl.mwator.footdemy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mwator.footdemy.model.Event;
import pl.mwator.footdemy.model.Post;
import pl.mwator.footdemy.service.EventService;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/event")
public class EventController {
    private final EventService eventService;

    @Autowired
    public EventController(EventService eventService) {
        this.eventService = eventService;
    }
    @RequestMapping(value = "/get/all", method = RequestMethod.GET)
    public ResponseEntity<?> getEvents(){
        return new ResponseEntity<Object>(eventService.getEvents(), HttpStatus.OK);
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getEvent(@PathVariable int id){
        return new ResponseEntity<Object>(eventService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteEvent(@PathVariable int id){
        eventService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<?> createEvent(@RequestBody Event event){
        eventService.create(event);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> editEvent(@PathVariable int id, @RequestBody Post event){
        System.out.println("Updating Event " + id);

        Event currentEvent = eventService.get(id);

        if (currentEvent == null) {
            System.out.println("Event with id " + id + " not found");
            return new ResponseEntity<Event>(HttpStatus.NOT_FOUND);
        }

        currentEvent.setTitle(event.getTitle());

        eventService.update(currentEvent);

        return new ResponseEntity<>(eventService.update(currentEvent), HttpStatus.OK);
    }
}
