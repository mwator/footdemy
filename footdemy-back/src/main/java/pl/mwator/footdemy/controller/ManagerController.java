package pl.mwator.footdemy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mwator.footdemy.model.Manager;
import pl.mwator.footdemy.service.ManagerService;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/manager")
public class ManagerController {
    private final ManagerService managerService;

    @Autowired
    public ManagerController(ManagerService managerService) {
        this.managerService = managerService;
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getManager(@PathVariable int id){
        return new ResponseEntity<Object>(managerService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<?> createManager(@RequestBody Manager manager){
        managerService.create(manager);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
