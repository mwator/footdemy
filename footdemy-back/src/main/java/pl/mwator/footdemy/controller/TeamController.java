package pl.mwator.footdemy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mwator.footdemy.model.Team;
import pl.mwator.footdemy.service.TeamService;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/team")
public class TeamController {
    private final TeamService teamService;

    @Autowired
    public TeamController(TeamService teamService) {
        this.teamService = teamService;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<?> createTeam(@RequestBody Team team){
        teamService.create(team);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getTeam(@PathVariable int id){
        return new ResponseEntity<Object>(teamService.get(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteTeam(@PathVariable int id){
        teamService.deleteById(id);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @RequestMapping(value = "/get/all", method = RequestMethod.GET)
    public ResponseEntity<?> getTeams(){
        return new ResponseEntity<Object>(teamService.getTeams(), HttpStatus.OK);
    }

    @RequestMapping(value = "/get/{id}/allplayers", method = RequestMethod.GET)
    public ResponseEntity<?> getTeamPlayers(@PathVariable int id){
        return new ResponseEntity<Object>(teamService.get(id).getTeamsPlayers(), HttpStatus.OK);
    }
}
