package pl.mwator.footdemy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mwator.footdemy.service.CoachService;
import pl.mwator.footdemy.service.ManagerService;
import pl.mwator.footdemy.service.PlayerService;
@CrossOrigin
@RequestMapping(value = "/api")
@RestController
public class LoginController {
    private final PlayerService playerService;
    private final CoachService coachService;
    private final ManagerService managerService;

    @Autowired
    public LoginController(PlayerService playerService,
                           CoachService coachService,
                           ManagerService managerService) {
        this.playerService = playerService;
        this.coachService = coachService;
        this.managerService = managerService;
    }

    @RequestMapping(
            value = "/login/{username}/{password}",
            method = RequestMethod.GET)
    public ResponseEntity<?> login(@PathVariable String username, @PathVariable String password) {
        if(playerService.findPlayerByUsernameAndPassword(username, password) != null){
            return new ResponseEntity<Object>(playerService
                    .findPlayerByUsernameAndPassword(username, password), HttpStatus.OK);
        } else if (coachService.findCoachByUsernameAndPassword(username, password) != null){
            return new ResponseEntity<Object>(coachService
                    .findCoachByUsernameAndPassword(username, password), HttpStatus.OK);
        } else if(managerService.findManagerByUsernameAndPassword(username, password) != null) {
            return new ResponseEntity<Object>(managerService
                    .findManagerByUsernameAndPassword(username, password), HttpStatus.OK);
        } else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}