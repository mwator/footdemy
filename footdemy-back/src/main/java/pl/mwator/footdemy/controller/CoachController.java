package pl.mwator.footdemy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mwator.footdemy.service.CoachService;

@RestController
@CrossOrigin
@RequestMapping("/api/coach")
public class CoachController {
    private CoachService coachService;

    @Autowired
    public CoachController(CoachService coachService) {
        this.coachService = coachService;
    }

    @RequestMapping(value = "/get/all", method = RequestMethod.GET)
    public ResponseEntity<?> getCoaches(){
        return new ResponseEntity<Object>(coachService.getCoaches(), HttpStatus.OK);
    }
    @RequestMapping(value = "/get/{username}/{password}", method = RequestMethod.GET)
    public ResponseEntity<?> getUserByLoginAndPassword(@PathVariable String username, @PathVariable String password){
        return new ResponseEntity<Object>(coachService.findCoachByUsernameAndPassword(username, password), HttpStatus.OK);
    }
}
