package pl.mwator.footdemy.service;

import pl.mwator.footdemy.model.Player;

import java.util.List;

public interface PlayerService {
    List<Player> getPlayers();
    Player get(int id);
    Player create(Player player);
    void delete(Player player);
    void deleteById(int id);
    Player update(Player player);
    Player findPlayerByUsernameAndPassword(String username, String password);
    List<Player> findPlayersByTeam_Id(int id);
    List<Player> findPlayersByTeamIdIs(int id);
}
