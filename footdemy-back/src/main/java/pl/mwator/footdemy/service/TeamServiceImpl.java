package pl.mwator.footdemy.service;

import org.apache.commons.collections4.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mwator.footdemy.model.Team;
import pl.mwator.footdemy.repository.TeamRepository;

import java.util.Iterator;
import java.util.List;

@Service
public class TeamServiceImpl implements TeamService {

    private final TeamRepository teamRepository;

    @Autowired
    public TeamServiceImpl(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @Override
    public List<Team> getTeams() {
        Iterator<Team> iterator = teamRepository.findAll().iterator();
        return IteratorUtils.toList(iterator);
    }

    @Override
    public Team get(int id) {
        return teamRepository.findOne(id);
    }

    @Override
    public Team create(Team team) {
        return teamRepository.save(team);
    }

    @Override
    public void delete(Team team) {
        teamRepository.delete(team);
    }

    @Override
    public void deleteById(int id) {
        teamRepository.delete(id);
    }

    @Override
    public Team update(Team team) {
        return teamRepository.save(team);
    }
}
