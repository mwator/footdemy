package pl.mwator.footdemy.service;

import pl.mwator.footdemy.model.Coach;

import java.util.List;

public interface CoachService {
    List<Coach> getCoaches();
    Coach get(int id);
    Coach create(Coach coach);
    void delete(Coach coach);
    void deleteById(int id);
    Coach update(Coach coach);
    Coach findCoachByUsernameAndPassword(String username, String password);
}
