package pl.mwator.footdemy.service;

import org.apache.commons.collections4.IteratorUtils;
import org.springframework.stereotype.Service;
import pl.mwator.footdemy.model.Application;
import pl.mwator.footdemy.repository.ApplicationRepository;

import java.util.Iterator;
import java.util.List;

@Service
public class ApplicationServiceImpl implements ApplicationService {

    private final ApplicationRepository applicationRepository;

    public ApplicationServiceImpl(ApplicationRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }

    @Override
    public List<Application> getApplications() {
        Iterator<Application> iterator = applicationRepository.findAll().iterator();
        return IteratorUtils.toList(iterator);
    }

    @Override
    public Application get(int id) {
        return applicationRepository.findOne(id);
    }

    @Override
    public Application create(Application application) {
        return applicationRepository.save(application);
    }

    @Override
    public void delete(Application application) {
        applicationRepository.delete(application);
    }

    @Override
    public void deleteById(int id) {
        applicationRepository.delete(id);
    }
}
