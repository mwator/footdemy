package pl.mwator.footdemy.service;

import pl.mwator.footdemy.model.Manager;
public interface ManagerService {
    Manager create(Manager manager);
    Manager get(int id);
    Manager findManagerByUsernameAndPassword(String username, String password);
}
