package pl.mwator.footdemy.service;

import pl.mwator.footdemy.model.Team;

import java.util.List;

public interface TeamService {
    List<Team> getTeams();
    Team get(int id);
    Team create(Team team);
    void delete(Team team);
    void deleteById(int id);
    Team update(Team team);
}
