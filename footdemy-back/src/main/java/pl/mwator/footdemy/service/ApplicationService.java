package pl.mwator.footdemy.service;

import pl.mwator.footdemy.model.Application;

import java.util.List;

public interface ApplicationService {
    List<Application> getApplications();
    Application get(int id);
    Application create(Application application);
    void delete(Application application);
    void deleteById(int id);
}
