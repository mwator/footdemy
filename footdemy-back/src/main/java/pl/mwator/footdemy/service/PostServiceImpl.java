package pl.mwator.footdemy.service;

import org.apache.commons.collections4.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mwator.footdemy.model.Post;
import pl.mwator.footdemy.repository.PostRepository;

import java.util.Iterator;
import java.util.List;

@Service
public class PostServiceImpl implements PostService {
    private final PostRepository postRepository;

    @Autowired
    public PostServiceImpl(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @Override
    public List<Post> getPosts() {
        Iterator<Post> iterator = postRepository.findAll().iterator();
        return IteratorUtils.toList(iterator);
    }

    @Override
    public Post get(int id) {
        return postRepository.findOne(id);
    }

    @Override
    public Post create(Post post) {
        return postRepository.save(post);
    }

    @Override
    public Post update(Post post) {
        return postRepository.save(post);
    }

    @Override
    public void delete(Post post) {
        postRepository.delete(post);
    }

    @Override
    public void deleteById(int id) {
        postRepository.delete(id);
    }
}
