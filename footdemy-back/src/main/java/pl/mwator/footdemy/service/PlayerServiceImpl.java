package pl.mwator.footdemy.service;

import org.apache.commons.collections4.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mwator.footdemy.model.Player;
import pl.mwator.footdemy.repository.PlayerRepository;

import java.util.Iterator;
import java.util.List;

@Service
public class PlayerServiceImpl implements PlayerService {
    private final PlayerRepository playerRepository;

    @Autowired
    public PlayerServiceImpl(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @Override
    public List<Player> getPlayers() {
        Iterator<Player> iterator = playerRepository.findAll().iterator();
        return IteratorUtils.toList(iterator);
    }

    @Override
    public Player get(int id) {
        return playerRepository.findOne(id);
    }

    @Override
    public Player create(Player player) {
        return playerRepository.save(player);
    }

    @Override
    public void delete(Player player) {
        playerRepository.delete(player);
    }

    @Override
    public void deleteById(int id) {
        playerRepository.delete(id);
    }

    @Override
    public Player update(Player player) {
        return playerRepository.save(player);
    }

    @Override
    public Player findPlayerByUsernameAndPassword(String username, String password) {
        return playerRepository.findPlayerByUsernameAndPassword(username, password);
    }

    @Override
    public List<Player> findPlayersByTeam_Id(int id) {
        Iterator<Player> iterator = playerRepository.findPlayersByTeam_Id(id).iterator();
        return IteratorUtils.toList(iterator);
    }
    @Override
    public List<Player> findPlayersByTeamIdIs(int id) {
        Iterator<Player> iterator = playerRepository.findPlayersByTeamIdIs(id).iterator();
        return IteratorUtils.toList(iterator);
    }
}
