package pl.mwator.footdemy.service;

import pl.mwator.footdemy.model.Post;

import java.util.List;

public interface PostService {
    List<Post> getPosts();
    Post get(int id);
    Post create(Post post);
    Post update(Post post);
    void delete(Post post);
    void deleteById(int id);
}
