package pl.mwator.footdemy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mwator.footdemy.model.Manager;
import pl.mwator.footdemy.repository.ManagerRepository;

@Service
public class ManagerServiceImpl implements ManagerService {
    private final ManagerRepository managerRepository;

    @Autowired
    public ManagerServiceImpl(ManagerRepository managerRepository) {
        this.managerRepository = managerRepository;
    }

    @Override
    public Manager create(Manager manager) {
        return managerRepository.save(manager);
    }

    @Override
    public Manager get(int id) {
        return managerRepository.findOne(id);
    }

    @Override
    public Manager findManagerByUsernameAndPassword(String username, String password) {
        return managerRepository.findByUsernameAndPassword(username, password);
    }
}
