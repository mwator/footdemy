package pl.mwator.footdemy.service;

import pl.mwator.footdemy.model.User;

import java.util.List;

public interface UserService {
    List<User> getUsers();
    User get(int id);
    User create(User user);
    User update(User user);
    void delete(User user);
    void deleteById(int id);
    User findUserByUsernameAndPassword(String username, String password);
}
