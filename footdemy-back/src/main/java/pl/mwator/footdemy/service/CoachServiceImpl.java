package pl.mwator.footdemy.service;

import org.apache.commons.collections4.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mwator.footdemy.model.Coach;
import pl.mwator.footdemy.repository.CoachRepository;

import java.util.Iterator;
import java.util.List;

@Service
public class CoachServiceImpl implements CoachService{

    private CoachRepository coachRepository;

    @Autowired
    public CoachServiceImpl(CoachRepository coachRepository) {
        this.coachRepository = coachRepository;
    }

    @Override
    public List<Coach> getCoaches() {
        Iterator<Coach> iterator = coachRepository.findAll().iterator();
        return IteratorUtils.toList(iterator);
    }

    @Override
    public Coach get(int id) {
        return coachRepository.findOne(id);
    }

    @Override
    public Coach create(Coach coach) {
        return coachRepository.save(coach);
    }

    @Override
    public void delete(Coach coach) {
        coachRepository.delete(coach);
    }

    @Override
    public void deleteById(int id) {
        coachRepository.delete(id);
    }

    @Override
    public Coach update(Coach coach) {
        return coachRepository.save(coach);
    }

    @Override
    public Coach findCoachByUsernameAndPassword(String username, String password) {
        return coachRepository.findCoachByUsernameAndPassword(username, password);
    }
}
