package pl.mwator.footdemy.service;

import pl.mwator.footdemy.model.Event;

import java.util.List;

public interface EventService {
    List<Event> getEvents();
    Event get(int id);
    Event create(Event event);
    Event update(Event event);
    void delete(Event event);
    void deleteById(int id);
}
