package pl.mwator.footdemy.model;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "COACH")
public class Coach {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "coach_id")
    private int id;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    private String username;
    @NotNull
    private String password;
    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.COACH;
    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "teamCoaches")
    private List<Team> coachTeams = new ArrayList<>();

    public Coach(String firstName, String lastName, String username, String password, List<Team> coachTeams) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.coachTeams = coachTeams;
    }
}
