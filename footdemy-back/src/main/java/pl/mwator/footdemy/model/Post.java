package pl.mwator.footdemy.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
@Table(name = "POST")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "post_id")
    private int id;
    @NotNull
    private String title;
    @NotNull
    private String message;
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    public Post(String title, String message, Date createdAt) {
        this.title = title;
        this.message = message;
        this.createdAt = createdAt;
    }
}
