package pl.mwator.footdemy.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
@Table(name = "EVENT")
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "event_id")
    private int id;
    @NotNull
    private String title;
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date start;
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date end;

    public Event(String title, Date start, Date end) {
        this.title = title;
        this.start = start;
        this.end = end;
    }
}
