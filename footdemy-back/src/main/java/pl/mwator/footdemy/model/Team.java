package pl.mwator.footdemy.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "TEAM")
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "team_id")
    private int id;
    private String name;
    private String description;
    @OneToMany(mappedBy = "team",cascade = CascadeType.ALL)
    @JsonIgnoreProperties(value = "team", allowSetters = true)
    private List<Player> teamsPlayers = new ArrayList<>();
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "Coach_Team",
            joinColumns = { @JoinColumn(name = "team_id") },
            inverseJoinColumns = { @JoinColumn(name = "coach_id") }
    )
    @JsonIgnoreProperties("coachTeams")
    private List<Coach> teamCoaches;

    public Team(String name, String description, List<Player> teamsPlayers, List<Coach> teamCoaches) {
        this.name = name;
        this.description = description;
        this.teamsPlayers = teamsPlayers;
        this.teamCoaches = teamCoaches;
    }
    public Team(int id, String name, String description, List<Player> teamsPlayers, List<Coach> teamCoaches){
        this.id = id;
        this.name = name;
        this.description = description;
        this.teamsPlayers = teamsPlayers;
        this.teamCoaches = teamCoaches;
    }
    public Team(int id){
        this.id = id;
    }

}
