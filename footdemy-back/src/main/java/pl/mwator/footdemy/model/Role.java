package pl.mwator.footdemy.model;

public enum Role {
    PLAYER, COACH, MANAGER
}
