package pl.mwator.footdemy.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@Entity
@Table(name = "PLAYER")
/*@JsonIgnoreProperties(value = {"team"},
        allowGetters = true)*/
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "player_id")
    private int id;
    @NotNull
    private String username;
    @NotNull
    private String password;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    private String position;
    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.PLAYER;
    @ManyToOne
    @JoinColumn(name="team_id")
    @JsonIgnoreProperties("teamsPlayers")
    @JsonDeserialize(as = Team.class)
    private Team team;

    public Player(String username, String password, String firstName, String lastName, Team team) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.team = team;
    }

}
