package pl.mwator.footdemy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FootdemyApplication {

	public static void main(String[] args) {
		SpringApplication.run(FootdemyApplication.class, args);
	}
}
