package pl.mwator.footdemy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.mwator.footdemy.model.Coach;

import javax.transaction.Transactional;

@Transactional
public interface CoachRepository extends JpaRepository<Coach, Integer>{
    Coach findCoachByUsernameAndPassword(String username, String password);
}
