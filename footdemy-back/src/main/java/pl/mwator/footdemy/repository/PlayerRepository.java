package pl.mwator.footdemy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.mwator.footdemy.model.Player;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface PlayerRepository extends JpaRepository<Player, Integer> {
    Player findPlayerByUsernameAndPassword(String username, String password);
    List<Player> findPlayersByTeam_Id(int id);
    List<Player> findPlayersByTeamIdIs(int id);
}
