package pl.mwator.footdemy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.mwator.footdemy.model.User;

import javax.transaction.Transactional;

@Transactional
public interface UserRepository extends JpaRepository<User, Integer> {
    User findUserByUsernameAndPassword(String username, String password);
}
