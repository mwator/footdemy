package pl.mwator.footdemy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.mwator.footdemy.model.Event;

import javax.transaction.Transactional;

@Transactional
public interface EventRepository extends JpaRepository<Event, Integer> {
}
