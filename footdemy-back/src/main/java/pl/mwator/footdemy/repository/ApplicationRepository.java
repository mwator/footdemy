package pl.mwator.footdemy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.mwator.footdemy.model.Application;

import javax.transaction.Transactional;

@Transactional
public interface ApplicationRepository extends JpaRepository<Application, Integer> {
}
