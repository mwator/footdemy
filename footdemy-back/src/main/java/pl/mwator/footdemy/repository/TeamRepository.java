package pl.mwator.footdemy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.mwator.footdemy.model.Team;

import javax.transaction.Transactional;

@Transactional
public interface TeamRepository extends JpaRepository<Team, Integer> {
}
