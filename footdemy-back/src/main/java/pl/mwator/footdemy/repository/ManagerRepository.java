package pl.mwator.footdemy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.mwator.footdemy.model.Manager;

import javax.transaction.Transactional;

@Transactional
public interface ManagerRepository extends JpaRepository<Manager, Integer>{
    Manager findByUsernameAndPassword(String username, String password);
}
