package pl.mwator.footdemy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.mwator.footdemy.model.Post;

import javax.transaction.Transactional;

@Transactional
public interface PostRepository extends JpaRepository<Post, Integer> {
}
