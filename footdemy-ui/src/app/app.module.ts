import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {UserService} from "./shared/user.service";
import { MessagesComponent } from './messages/messages.component';
import { MessageService } from './services/message.service';
import { AppRoutingModule } from './app-routing.module';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { MaterialModule } from './modules/material.module';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { TopToolbarComponent } from './top-toolbar/top-toolbar.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import {NgbModalModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';

import 'hammerjs';
import { LoginComponent } from './auth/login/login.component';
import { HomeComponent } from './home/home.component';
import {AuthService} from "./auth/auth.service";
import { CoachPageComponent } from './coaches/coach-page/coach-page.component';
import { CoachComponent } from './coaches/coach/coach.component';
import { PlayerListComponent } from './players/player-list/player-list.component';
import { PlayerDetailComponent } from './players/player-detail/player-detail.component';
import {PlayerService} from "./players/shared/player.service";
import {CoachService} from "./coaches/shared/coach.service";
import { PlayerCreateComponent } from './players/player-create/player-create.component';
import { TeamListComponent } from './teams/team-list/team-list.component';
import {TeamService} from "./teams/shared/team.service";
import { TeamCreateComponent } from './teams/team-create/team-create.component';
import { TeamDetailComponent } from './teams/team-detail/team-detail.component';
import { CoachDetailComponent } from './coaches/coach-detail/coach-detail.component';
import { CoachListComponent } from './coaches/coach-list/coach-list.component';
import { CoachCreateComponent } from './coaches/coach-create/coach-create.component';
import {LoginService} from "./auth/login.service";
import {AdminGuard} from "./auth/guards/admin.guard";
import {CoachGuard} from "./auth/guards/coach.guard";
import {PlayerGuard} from "./auth/guards/player.guard";
import { FooterComponent } from './footer/footer.component';
import { ManagerHomeComponent } from './managers/manager-home/manager-home.component';
import { NewApplicationComponent } from './new-application/new-application.component';
import {ApplicationService} from "./shared/application.service";
import { ApplicationsReceivedComponent } from './managers/applications-received/applications-received.component';
import {DatePipe} from "@angular/common";
import { ApplicationDetailComponent } from './managers/application-detail/application-detail.component';
import { PostListComponent } from './posts/post-list/post-list.component';
import { PostDetailComponent } from './posts/post-detail/post-detail.component';
import { PostCreateComponent } from './posts/post-create/post-create.component';
import {PostService} from "./posts/shared/post.service";
import {CalendarModule} from "angular-calendar";
import {DateTimePickerComponent} from "./calendar/calendar-utils/date-time-picker.component";
import {CalendarHeaderComponent} from "./calendar/calendar-utils/calendar-header.component";
import { CalendarViewComponent } from './calendar/calendar-view/calendar-view.component';
import { CalendarEditComponent } from './calendar/calendar-edit/calendar-edit.component';
import { CalendarEventsComponent } from './calendar/calendar-events/calendar-events.component';
import {CalendarService} from "./calendar/shared/calendar.service";


@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    TopToolbarComponent,
    LoginComponent,
    HomeComponent,
    CoachPageComponent,
    CoachComponent,
    PlayerListComponent,
    PlayerDetailComponent,
    PlayerCreateComponent,
    TeamListComponent,
    TeamCreateComponent,
    TeamDetailComponent,
    CoachDetailComponent,
    CoachListComponent,
    CoachCreateComponent,
    FooterComponent,
    ManagerHomeComponent,
    NewApplicationComponent,
    ApplicationsReceivedComponent,
    ApplicationDetailComponent,
    PostListComponent,
    PostDetailComponent,
    PostCreateComponent,
    CalendarHeaderComponent,
    DateTimePickerComponent,
    CalendarViewComponent,
    CalendarEditComponent,
    CalendarEventsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    AngularFontAwesomeModule,
    FlexLayoutModule,
    NgbModule.forRoot(),
    NgbModalModule.forRoot(),
    CalendarModule.forRoot()
  ],
  providers: [
    UserService,
    MessageService,
    AuthService,
    PlayerService,
    CoachService,
    TeamService,
    LoginService,
    ApplicationService,
    PostService,
    AdminGuard,
    CoachGuard,
    PlayerGuard,
    DatePipe,
    CalendarService
  ],
  bootstrap: [AppComponent],
  exports: [CalendarHeaderComponent, DateTimePickerComponent]
})
export class AppModule { }
