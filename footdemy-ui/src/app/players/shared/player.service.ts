import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Player} from "../../shared/player.model";
import {environment} from "../../../environments/environment";
import "rxjs/add/operator/map";

@Injectable()
export class PlayerService {

  constructor(private http: HttpClient) { }

  getPlayers(): Observable<Player[]>{
    return this.http.get<Player[]>(environment.apiEndpoint + '/player/get/all');
  }

  getPlayer(id: number): Observable<Player> {
    return this.http
      .get<Player>(environment.apiEndpoint + '/player/get/' + id);
  }

  getPlayersByTeam(id: number): Observable<Player[]> {
    return this.http
      .get<Player[]>(environment.apiEndpoint + '/getbyteam/{id}/' + id);
  }

  delete(id: number): Observable<Player> {
    return this.http.delete<Player>(environment.apiEndpoint + '/player/get/' + id);
  }

  edit(player: Player): Observable<Player>{
    return this.http
      .put<Player>(environment.apiEndpoint + '/player/edit/' + player.id, player);
  }

  create(player: Player): Observable<Player>{
    return this.http
      .post<Player>(environment.apiEndpoint + '/player/create', player);
  }
}
