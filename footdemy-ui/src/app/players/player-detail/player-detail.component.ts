import { Component, OnInit } from '@angular/core';
import {Player} from "../../shared/player.model";
import {PlayerService} from "../shared/player.service";
import {ActivatedRoute, Router} from "@angular/router";
import 'rxjs/add/operator/switchMap';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-player-detail',
  templateUrl: './player-detail.component.html',
  styleUrls: ['./player-detail.component.css']
})
export class PlayerDetailComponent implements OnInit {

  player: Player;
  editPlayer = false;
  confirmDelete = false;
  confirmEdit = false;
  playerFormGroup: FormGroup;
  playerEditedSuccess = false;

  constructor(private playerService: PlayerService,
              private router: Router,
              private formBuilder: FormBuilder,
              private route: ActivatedRoute) {
    this.playerFormGroup = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.minLength(2)]]
    })
  }

  ngOnInit() {
    this.route.paramMap.switchMap(params => this.playerService.getPlayer(+params.get('id')))
      .subscribe(player => this.player = player);
  }

  edit(){
    this.editPlayer = true;
  }

  abortEdit(){
    this.confirmEdit = false;
  }

  editConfirmed(){
    const values = this.playerFormGroup.value;
    console.log(values.firstName);

    const playerEdited: Player = {
      id: this.player.id,
      firstName: values.firstName,
      lastName: values.lastName,
    };

    this.playerService.edit(playerEdited).subscribe(() =>{
      this.playerFormGroup.reset();
      this.editPlayer = false;
      this.playerEditedSuccess = true;
      setTimeout(() => {
        this.playerEditedSuccess = false;
      }, 3000)
    });
  }

  delete(){
    this.confirmDelete = true;
  }

  abortDelete(){
    this.confirmDelete = false;
  }

  deleteConfirmed(){
    this.playerService.delete(this.player.id).subscribe(player => this.router
      .navigateByUrl("/coach-page"));
  }

  closeAlert(){
    this.playerEditedSuccess = false;
  }
}
