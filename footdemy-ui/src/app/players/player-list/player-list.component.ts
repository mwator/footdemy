import { Component, OnInit } from '@angular/core';
import {Player} from "../../shared/player.model";
import {PlayerService} from "../shared/player.service";
import {Router} from "@angular/router";
import {User} from "../../shared/user.model";
import {AuthService} from "../../auth/auth.service";

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.css']
})
export class PlayerListComponent implements OnInit {

  players: Player[];
  playerToDelete: Player;
  player: Player;

  user: User;
  constructor(private playerService: PlayerService,
              private router: Router,
              private authService: AuthService) { }

  ngOnInit() {
    this.playerService.getPlayers().subscribe(players => {
      this.players = players;
    });
    this.user = this.authService.currentUser();
  }

  details(player: Player){
    this.router.navigateByUrl('/player/' + player.id);
    console.log('details clicked ' + player.id);
  }
  delete(player: Player, $event){
    console.log("delete clicked");
    this.playerToDelete = player;
    $event.stopPropagation();
  }

  deleteAbort($event){
    this.playerToDelete = null;
    $event.stopPropagation();
  }
  deleteConfirmed($event){
    this.playerService.delete(this.playerToDelete.id)
      .switchMap(player => this.playerService.getPlayers())
      .subscribe( players => {
        this.players = players;
      });
    $event.stopPropagation();
  }

  createPlayer(){
    this.router.navigateByUrl('/players/create');
  }
}
