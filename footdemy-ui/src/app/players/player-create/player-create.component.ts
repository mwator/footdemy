import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PlayerService} from "../shared/player.service";
import {Player} from "../../shared/player.model";
import {TeamService} from "../../teams/shared/team.service";
import {Team} from "../../shared/team.model";
import {Md5} from "ts-md5";

@Component({
  selector: 'app-player-create',
  templateUrl: './player-create.component.html',
  styleUrls: ['./player-create.component.css']
})
export class PlayerCreateComponent implements OnInit {

  playerFormGroup: FormGroup;
  playerCreatedSuccess = false;
  teams: Team[];

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private playerService: PlayerService,
              private teamService: TeamService) {
    this.playerFormGroup = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.minLength(2)]],
      username: ['', [Validators.required, Validators.minLength(2)]],
      password: ['', [Validators.required, Validators.minLength(2)]],
      team: ['']
    })
  }

  ngOnInit() {
    this.teamService.getTeams().subscribe(teams => {
      this.teams = teams;
    })
  }

  back(){
    this.router.navigateByUrl('/coach-page');
  }

  isValid(controlName: string){
    const control = this.playerFormGroup.controls[controlName];
    return !control.invalid && (control.touched || control.dirty);
  }

  isInvalid(controlName: string){
    const control = this.playerFormGroup.controls[controlName];
    return control.invalid && (control.touched || control.dirty);
  }

  onSubmit(){
    const values = this.playerFormGroup.value;
    const player: Player = {
      firstName: values.firstName,
      lastName: values.lastName,
      username: values.username,
      password: Md5.hashStr(values.password).toString(),
      team: values.team
    };
    this.playerService.create(player).subscribe(() => {
      this.playerFormGroup.reset();
      this.playerCreatedSuccess = true;
      setTimeout(() => {
        this.playerCreatedSuccess = false;
      }, 3000)
    });
  }

  closeAlert(){
    this.playerCreatedSuccess = false;
  }
}
