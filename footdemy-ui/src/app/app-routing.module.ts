import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from "./home/home.component";
import {LoginComponent} from "./auth/login/login.component";
import {CoachPageComponent} from "./coaches/coach-page/coach-page.component";
import {PlayerDetailComponent} from "./players/player-detail/player-detail.component";
import {PlayerCreateComponent} from "./players/player-create/player-create.component";
import {TeamListComponent} from "./teams/team-list/team-list.component";
import {TeamCreateComponent} from "./teams/team-create/team-create.component";
import {TeamDetailComponent} from "./teams/team-detail/team-detail.component";
import {CoachListComponent} from "./coaches/coach-list/coach-list.component";
import {CoachCreateComponent} from "./coaches/coach-create/coach-create.component";
import {CoachDetailComponent} from "./coaches/coach-detail/coach-detail.component";
import {AdminGuard} from "./auth/guards/admin.guard";
import {CoachGuard} from "./auth/guards/coach.guard";
import {ManagerHomeComponent} from "./managers/manager-home/manager-home.component";
import {NewApplicationComponent} from "./new-application/new-application.component";
import {ApplicationsReceivedComponent} from "./managers/applications-received/applications-received.component";
import {ApplicationDetailComponent} from "./managers/application-detail/application-detail.component";
import {PostListComponent} from "./posts/post-list/post-list.component";
import {PostCreateComponent} from "./posts/post-create/post-create.component";
import {CalendarViewComponent} from "./calendar/calendar-view/calendar-view.component";
import {PlayerGuard} from "./auth/guards/player.guard";


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'coach-page', component: CoachPageComponent },
  { path: 'player/:id', component: PlayerDetailComponent },
  { path: 'players/create', component: PlayerCreateComponent, canActivate: [CoachGuard] },
  { path: 'teams', component: TeamListComponent },
  { path: 'teams/create', component: TeamCreateComponent, canActivate: [AdminGuard] },
  { path: 'team/:id', component: TeamDetailComponent },
  { path: 'coaches', component: CoachListComponent },
  { path: 'coaches/create', component: CoachCreateComponent, canActivate: [AdminGuard] },
  { path: 'coach/:id', component: CoachDetailComponent },
  { path: 'manager', component: ManagerHomeComponent, canActivate: [AdminGuard]},
  { path: 'newuser', component: NewApplicationComponent },
  { path: 'applications', component: ApplicationsReceivedComponent, canActivate: [AdminGuard] },
  { path: 'application/:id', component: ApplicationDetailComponent, canActivate: [AdminGuard] },
  { path: 'posts', component: PostListComponent},
  { path: 'posts/create', component: PostCreateComponent },
  { path: 'calendar', component: CalendarViewComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
