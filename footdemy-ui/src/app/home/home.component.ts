import {Component, Input, OnInit} from '@angular/core';
import {User} from "../shared/user.model";
import {AuthService} from "../auth/auth.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  toolbarTitle = 'FootDemy';

  user: User;
  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.user = this.authService.currentUser();
/*    console.log(this.user.role);
    console.log('user ' + this.user);
    console.log(' ' + this.authService.currentUser());
    console.log('+get ' + this.authService.currentUser());*/
  }

}
