import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Manager} from "./manager.model";
import {Observable} from "rxjs/Observable";

@Injectable()
export class ManagerService {

  constructor(private http: HttpClient) { }

  get(id: number): Observable<Manager> {
    return this.http
      .get<Manager>(environment.apiEndpoint + '/manager/get/' + id);
  }

  create(manager: Manager): Observable<Manager>{
    return this.http
      .post<Manager>(environment.apiEndpoint + '/manager/create', manager);
  }
}
