import { Component, OnInit } from '@angular/core';
import {ApplicationService} from "../../shared/application.service";
import {Application} from "../../shared/application.model";
import {Router} from "@angular/router";

@Component({
  selector: 'app-applications-received',
  templateUrl: './applications-received.component.html',
  styleUrls: ['./applications-received.component.css']
})
export class ApplicationsReceivedComponent implements OnInit {

  applications: Application[];
  constructor(private router: Router,
              private applicationService: ApplicationService) { }

  ngOnInit() {
    this.applicationService.getAll().subscribe(applications => this.applications = applications);
  }

  details(application: Application){
    this.router.navigateByUrl('/application/' + application.id);
    console.log('details clicked ' + application.id);
  }
}
