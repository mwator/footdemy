import {Component, Input, OnInit} from '@angular/core';
import {Team} from "../../shared/team.model";
import {TeamService} from "../shared/team.service";
import {Router} from "@angular/router";
import {AuthService} from "../../auth/auth.service";
import {User} from "../../shared/user.model";

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css']
})
export class TeamListComponent implements OnInit {

  teams: Team[];
  teamToDelete: Team;
  team: Team;

  user: User;

  constructor(private teamService: TeamService,
              private router: Router,
              private authService: AuthService) {

  }

  ngOnInit() {
    this.teamService.getTeams().subscribe(teams => {
      this.teams = teams;
    });
    this.user = this.authService.currentUser();
  }

  details(team: Team){
    this.router.navigateByUrl('/team/' + team.id);
    console.log('details clicked ' + team.id);
  }

  delete(team: Team, $event){
    this.teamToDelete = team;
    $event.stopPropagation();
  }
  deleteAbort($event){
    this.teamToDelete = null;
    $event.stopPropagation();
  }
  createTeam(){
    this.router.navigateByUrl('/teams/create');
  }


}
