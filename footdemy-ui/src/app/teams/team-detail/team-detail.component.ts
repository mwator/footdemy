import { Component, OnInit } from '@angular/core';
import {Team} from "../../shared/team.model";
import {TeamService} from "../shared/team.service";
import {ActivatedRoute, Router} from "@angular/router";
import 'rxjs/add/operator/switchMap';
import {Player} from "../../shared/player.model";

@Component({
  selector: 'app-team-detail',
  templateUrl: './team-detail.component.html',
  styleUrls: ['./team-detail.component.css']
})
export class TeamDetailComponent implements OnInit {

  team: Team;
  confirmDelete = false;
  teamPlayers: Player[];


  constructor(private teamService: TeamService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.switchMap(params => this.teamService.getTeam(+params.get('id')))
      .subscribe(team => this.team = team);
    this.route.paramMap.switchMap(params => this.teamService.getTeamPlayers(+params.get('id')))
      .subscribe(players => this.teamPlayers = players);
  }

  delete(){
    this.confirmDelete = true;
  }

  abortDelete(){
    this.confirmDelete = false;
  }

  deleteConfirmed(){
    this.teamService.delete(this.team.id).subscribe(team => this.router
      .navigateByUrl("/teams"));
  }

}
