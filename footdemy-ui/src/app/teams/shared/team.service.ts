import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Team} from "../../shared/team.model";
import {Observable} from "rxjs/Observable";
import {Player} from "../../shared/player.model";

@Injectable()
export class TeamService {

  constructor(private http: HttpClient) { }

  create(team: Team): Observable<Team>{
    return this.http
      .post<Team>(environment.apiEndpoint + '/team/create', team);
  }

  getTeams(): Observable<Team[]>{
    return this.http.get<Team[]>(environment.apiEndpoint + '/team/get/all');
  }

  delete(id: number): Observable<Team> {
    return this.http.delete<Team>(environment.apiEndpoint + '/team/get/' + id);
  }

  getTeam(id: number): Observable<Team> {
    return this.http.get<Team>(environment.apiEndpoint + '/team/get/' + id);
  }
  getTeamPlayers(id: number): Observable<Player[]> {
    return this.http.get<Player[]>(environment.apiEndpoint + '/team/get/' + id + '/allplayers');
  }
}
