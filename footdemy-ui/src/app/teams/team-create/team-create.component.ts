import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {TeamService} from "../shared/team.service";
import {Team} from "../../shared/team.model";

@Component({
  selector: 'app-team-create',
  templateUrl: './team-create.component.html',
  styleUrls: ['./team-create.component.css']
})
export class TeamCreateComponent implements OnInit {

  teamFormGroup: FormGroup;
  teamCreatedSuccess = false;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private teamService: TeamService) {
    this.teamFormGroup = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      description: ['', [Validators.required, Validators.minLength(2)]]
    })
  }

  ngOnInit() {
  }

  back(){
    this.router.navigateByUrl('/teams')
  }

  isValid(controlName: string){
    const control = this.teamFormGroup.controls[controlName];
    return !control.invalid && (control.touched || control.dirty);
  }

  isInvalid(controlName: string){
    const control = this.teamFormGroup.controls[controlName];
    return control.invalid && (control.touched || control.dirty);
  }

  onSubmit(){
    const values = this.teamFormGroup.value;
    const team: Team = {
      name: values.name,
      description: values.description
    };
    this.teamService.create(team).subscribe(team => {
      this.teamFormGroup.reset();
      this.teamCreatedSuccess = true;
      setTimeout(() => {
        this.teamCreatedSuccess = false;
      }, 3000)
    });
  }

  closeAlert(){
    this.teamCreatedSuccess = false;
  }
}
