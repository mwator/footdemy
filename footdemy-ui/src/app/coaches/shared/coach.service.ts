import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Coach} from "./coach";
import {environment} from "../../../environments/environment";

@Injectable()
export class CoachService {

  constructor(private http: HttpClient) { }

  getCoaches(): Observable<Coach[]> {
    console.log(this.http.get<Coach[]>(environment.apiEndpoint + '/coach/get/all'));
    return this.http.get<Coach[]>(environment.apiEndpoint + '/coach/get/all');
  }
}
