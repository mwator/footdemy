export class Coach {
  id?: number;
  firstName: string;
  lastName: string;
  password: string;
  username: string;
  role: string;
}
