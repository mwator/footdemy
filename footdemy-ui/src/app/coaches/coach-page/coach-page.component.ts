import {Component, Input, OnInit} from '@angular/core';
import {Player} from "../../shared/player.model";
import {PlayerService} from "../../players/shared/player.service";

@Component({
  selector: 'app-coach-page',
  templateUrl: './coach-page.component.html',
  styleUrls: ['./coach-page.component.css']
})
export class CoachPageComponent implements OnInit {
  toolbarTitle = "Footdemy";
  players: Player[];

  constructor(private playerService: PlayerService) { }

  ngOnInit() {
    this.playerService.getPlayers().subscribe(players => {
      this.players = players;
    })
  }

}
