import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {User} from "./user.model";
import "rxjs/add/operator/map";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable()
export class UserService {


  constructor(private http: HttpClient) {
  }



  getUsers(): Observable<User[]> {
    console.log(this.http.get<User[]>(environment.apiEndpoint + '/users/all'));
    return this.http.get<User[]>(environment.apiEndpoint + '/users/all');

    /*      .pipe( tap(users => this.log('fetched users'))
     , catchError(this.handleError('getUsers', []))*/
  }

  getUser(username: string, password: string): Observable<User> {
    console.log(this.http.get<User>(environment.apiEndpoint + '/users/' + username + '/' + password));
    return this.http.get<User>(environment.apiEndpoint + '/users/' + username + '/' + password);
  }

  getById(id: number): Observable<User> {
    return this.http
      .get<User>(environment.apiEndpoint + '/users/' + id);
  }
  getPlayer(id: number): Observable<User> {
    return this.http
      .get<User>(environment.apiEndpoint + '/player/get/' + id);
  }
  delete(id: number): Observable<User> {
    return this.http.delete<User>(environment.apiEndpoint + '/player/get/' + id);
  }



/*  login(username: string, password: string) {
    return this.http.post<any>('/api/users/${username}/${password}', {username: username, password: password})
      .map(user => {
        // login successful if there's a jwt token in the response
        if (user) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          console.log(JSON.stringify(user));
          localStorage.setItem('currentUser', JSON.stringify(user));
        }

        return user;
      });
  }
 */
/*  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
  }*/


}
