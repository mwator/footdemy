import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {Application} from "./application.model";
import {Observable} from "rxjs/Observable";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class ApplicationService {

  constructor(private http: HttpClient) { }

  create(application: Application): Observable<Application>{
    return this.http
      .post<Application>(environment.apiEndpoint + '/application/create', application);
  }

  getAll(): Observable<Application[]>{
    return this.http.get<Application[]>(environment.apiEndpoint + '/application/get/all');
  }
}
