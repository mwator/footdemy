import {Team} from "./team.model";
export class Player{
  id?: number;
  firstName: string;
  lastName: string;
  username?: string;
  password?: string;
  teamId?: Team;
  team?: number;
  role?: string;
}
