import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {PostService} from "../shared/post.service";
import {DatePipe} from "@angular/common";
import {Post} from "../shared/post.model";

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent implements OnInit {

  postFormGroup: FormGroup;
  postCreatedSuccess = false;
  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private postService: PostService,
              private datePipe: DatePipe) {
    this.postFormGroup = this.formBuilder.group({
      title: ['', [Validators.required, Validators.minLength(2)]],
      message: ['', [Validators.required, Validators.minLength(10)]]
    })
  }
  ngOnInit() {
  }
  back(){
    this.router.navigateByUrl('/posts');
  }

  onSubmit(){
    const createdAt = this.datePipe.transform(Date.now(), "yyyy-MM-dd'T'HH:mm:ss.SSS");
    console.log(createdAt);
    const values = this.postFormGroup.value;
    const post: Post = {
      title: values.title,
      message: values.message,
      createdAt: createdAt
    };
    this.postService.create(post).subscribe(() => {
      this.postFormGroup.reset();
      this.postCreatedSuccess = true;
      setTimeout(() =>{
        this.postCreatedSuccess = false
      }, 3000)
    });
  }

  closeAlert(){
    this.postCreatedSuccess = false;
  }

}
