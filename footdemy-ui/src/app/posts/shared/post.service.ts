import { Injectable } from '@angular/core';
import {Post} from "./post.model";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs/Observable";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class PostService {

  constructor(private http: HttpClient) { }

  getPosts(): Observable<Post[]>{
    return this.http.get<Post[]>(environment.apiEndpoint + '/post/get/all');
  }
  create(post: Post): Observable<Post>{
    return this.http.post<Post>(environment.apiEndpoint + '/post/create', post);
  }

}
