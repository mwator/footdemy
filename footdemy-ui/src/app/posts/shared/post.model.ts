export class Post{
  id?: number;
  title: string;
  message: string;
  createdAt: string;
}
