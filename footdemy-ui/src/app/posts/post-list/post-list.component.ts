import { Component, OnInit } from '@angular/core';
import {Post} from "../shared/post.model";
import {Router} from "@angular/router";
import {PostService} from "../shared/post.service";

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {

  posts: Post[];
  constructor(private router: Router, private postService: PostService) { }

  ngOnInit() {
    this.postService.getPosts().subscribe(posts => this.posts = posts);
  }

  createPost(){
    this.router.navigateByUrl('posts/create');
  }

}
