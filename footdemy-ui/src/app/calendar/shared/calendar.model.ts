export class Calendar{
  id?: number;
  title: string;
  start: string;
  end: string;
  color?: string;
  draggable?: boolean;
  actions?: any;
  resizable?: any;
  beforeStart?: boolean;
  afterEnd?: boolean;
}
