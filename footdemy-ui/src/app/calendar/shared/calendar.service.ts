import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Calendar} from "./calendar.model";
import {environment} from "../../../environments/environment";

@Injectable()
export class CalendarService {

  constructor(private http: HttpClient) { }

  getCalendarEvents(): Observable<Calendar[]>{
    return this.http.get<Calendar[]>(environment.apiEndpoint + '/event/get/all');
  }

  delete(id: number): Observable<Calendar> {
    return this.http.delete<Calendar>(environment.apiEndpoint + '/player/get/' + id);
  }

  edit(calendar: Calendar): Observable<Calendar>{
    return this.http
      .put<Calendar>(environment.apiEndpoint + '/player/edit/' + calendar.id, calendar);
  }

  create(calendar: Calendar): Observable<Calendar>{
    return this.http
      .post<Calendar>(environment.apiEndpoint + '/player/create', calendar);
  }
}
