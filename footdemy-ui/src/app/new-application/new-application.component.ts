import {ApplicationRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ApplicationService} from "../shared/application.service";
import {Application} from "../shared/application.model";

@Component({
  selector: 'app-new-application',
  templateUrl: './new-application.component.html',
  styleUrls: ['./new-application.component.css']
})
export class NewApplicationComponent implements OnInit {

  applicationGroup: FormGroup;
  applicationSendSuccess = false;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private applicationService: ApplicationService) {
    this.applicationGroup = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.minLength(2)]],
      yearOfBirth: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4)]],
      phoneNumber: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(9)]]
    })
  }

  ngOnInit() {
  }
  onSubmit(){
    const values = this.applicationGroup.value;
    const application: Application = {
      firstName: values.firstName,
      lastName: values.lastName,
      yearOfBirth: values.yearOfBirth,
      phoneNumber: values.phoneNumber
    }
    this.applicationService.create(application).subscribe(()=>{
      this.applicationGroup.reset();
      this.applicationSendSuccess = true;
      setTimeout(() =>{
        this.applicationSendSuccess = false;
      }, 3000)
    });
  }
  closeAlert(){
    this.applicationSendSuccess = false;
  }
}
