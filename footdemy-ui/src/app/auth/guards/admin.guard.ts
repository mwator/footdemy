import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from "../auth.service";


@Injectable()
export class AdminGuard implements CanActivate {

  constructor(private router: Router,
              private authService: AuthService){
  }

  canActivate(): any {
    if (this.authService.currentUser().role == 'MANAGER') {
      return true;
    } else {
      // not logged in so redirect to login page with the return url
      this.router.navigateByUrl('/');
      return false;
    }
  }
}
