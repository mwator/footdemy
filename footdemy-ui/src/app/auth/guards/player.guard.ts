import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {User} from "../../shared/user.model";
import {AuthService} from "../auth.service";

@Injectable()
export class PlayerGuard implements CanActivate {

  user: User;
  constructor(private router: Router,
              private authService: AuthService){ }

  canActivate(): any {

    this.user = this.authService.currentUser();
    if (this.user.role === 'PLAYER') {
      return true;
    } else {
      // not logged in so redirect to login page with the return url
      this.router.navigateByUrl('/');
      return false;
    }

  }
}
