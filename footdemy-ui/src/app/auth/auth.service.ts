import {Injectable} from '@angular/core';
import {User} from "../shared/user.model";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/observable/of';
import {LoginService} from "./login.service";
import "rxjs/add/operator/map";
import {Md5} from "ts-md5";

@Injectable()
export class AuthService {

  userAccepted: any;
  constructor(private loginService: LoginService) {
  }
  login(user: User): Observable<any> {
    this.loginService.getUser(user.username, Md5.hashStr(user.password).toString()).subscribe(userA => {
      this.userAccepted = userA;
    });
    if(this.userAccepted){
      localStorage.setItem('currentUser', JSON.stringify(this.userAccepted));
      return Observable.of(this.userAccepted);
    } else {
      return Observable.of(null);
    }

  }

  logout(){
    return localStorage.removeItem('currentUser');
  }

  currentUser(){
    return JSON.parse(localStorage.getItem('currentUser'));
  }

}
