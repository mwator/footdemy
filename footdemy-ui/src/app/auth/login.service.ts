import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {User} from "../shared/user.model";
import {Observable} from "rxjs/Observable";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class LoginService {

  constructor(private http: HttpClient) { }

  getUser(username: string, password: string): Observable<any> {
    return this.http.get<any>(environment.apiEndpoint + '/login/' + username + '/' + password);
  }
}
