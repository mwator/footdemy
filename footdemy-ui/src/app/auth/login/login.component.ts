import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthService} from "../auth.service";
import {Subscription} from "rxjs/Subscription";
import {Observable} from "rxjs/Observable";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginGroup: FormGroup;
  request: Subscription;
  loginError: string;

  constructor(private router: Router,
              private authService: AuthService) {
    this.authService.logout();
    this.loginGroup = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.minLength(2)]),
      password: new FormControl('', [Validators.required, Validators.minLength(2)])
    });
  }

  ngOnInit() {
  }

  isValid(controlName: string){
    const control = this.loginGroup.controls[controlName];
    return !control.invalid && (control.touched || control.dirty);
  }

  isInvalid(controlName: string){
    const control = this.loginGroup.controls[controlName];
    return control.invalid && (control.touched || control.dirty);
  }

  login() {
    if(this.request){
      this.request.unsubscribe();
    }
    this.request = this.authService
      .login(this.loginGroup.value)
      .subscribe(lUser => {
        if(lUser){
          this.loginError = null;
          if(lUser.role === 'COACH'){
            this.router.navigate(['/coach-page']);
          }
          if(lUser.role === 'PLAYER'){
            this.router.navigate(['/']);
          }
          if(lUser.role === 'MANAGER'){
            this.router.navigate(['/manager']);
          }
        } else {
          this.loginError = "username and password was wrong";
        }
      });
  }

  ngOnDestroy(){
    if(this.request){
      this.request.unsubscribe();
    }
  }
}
