import {Component, Input, OnInit} from '@angular/core';
import {User} from "../shared/user.model";
import {AuthService} from "../auth/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-top-toolbar',
  templateUrl: './top-toolbar.component.html',
  styleUrls: ['./top-toolbar.component.css']
})
export class TopToolbarComponent implements OnInit {

  @Input()
  title: string;

  user: User;

  constructor(private authService: AuthService,
              private router: Router) {
    this.user = authService.currentUser();
  }

  logout(){
    this.router.navigate(["/login"]).then(() => console.log('logged out'));
  }
  ngOnInit() {
  }

}
